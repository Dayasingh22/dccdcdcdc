import React from 'react';
import cardImage from '../Header/img.png'
import '../../App.css';

const MainShowcase = () => {
    return(
        <div className="main-items">
        <div className="search">
            <div className="search-box">
                <input type="text" className="form-control" placeholder="Search recipes and more" />
            </div>
            <div>
            <div className="dropdown">
                <button className="btn btn-dark dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Sort by: Newest
                </button>
                <div className="dropdown-menu" aria-labelledby="dropdownMenuButton">
                    <li className="dropdown-item">Oldest</li>
                    <li className="dropdown-item">Fresh</li>
                    <li className="dropdown-item">Newly Added</li>
                </div>
                </div>
            </div>
        </div>

        <div className="items">
            <div className="card">
                <img src={cardImage} alt="showcase" />
                <div className="card-body">
                    <h5>Pan-Cakes</h5>
                    <div className="rating">
                        <p>4.7 (23)</p>
                        <p className="author">by SARAH</p>
                    </div>
                </div>
            </div>
            <div className="card">
                <img src={cardImage} alt="showcase" />
                <div className="card-body">
                    <h5>Chocolate Cakes</h5>
                    <div className="rating">
                        <p>4.7 (23)</p>
                        <p className="author">by SARAH</p>
                    </div>
                </div>
            </div>
            <div className="card">
                <img src={cardImage} alt="showcase" />
                <div className="card-body">
                    <h5>Avocado</h5>
                    <div className="rating">
                        <p>4.7 (23)</p>
                        <p className="author">by SARAH</p>
                    </div>
                </div>
            </div>
        </div>

        
    </div>
    )
}

export default MainShowcase;