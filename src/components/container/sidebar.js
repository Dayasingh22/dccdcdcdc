import React from 'react';
import '../../App.css';

const SideBar = () => {
    return(
        <div className="side-bar">
            <h3>Recipes</h3>
            <div className="filter">
                <h6>Filter by:</h6>
                <p>Clear filter</p>
            </div>
            <div className="filter-items">
                <div className="filter-items-listing">
                    <ul className="filter-items-list">
                        <li>Contest Winners</li>
                        <li>Featured</li>
                        <li>Test Kitchen Approved</li>
                    </ul>
                </div>
                <div className="filter-items-listing">
                    <ul className="filter-items-list-2">
                        <li><input type="checkbox" /></li>
                        <li><input type="checkbox" /></li>
                        <li><input type="checkbox" /></li>
                    </ul>
                </div>
            </div>
        </div>
        
    )
}

export default SideBar;