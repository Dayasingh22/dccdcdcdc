import React from 'react';
import '../../App.css';

const NavBar = () => {
    return(
        <header className="food-app-header">
            <div className="nav-bar">
                <h1>Foodima</h1>
            </div>
            <div className="list">
                <ul className="nav-bar-list">
                    <li>Shop</li>
                    <li>Features</li>
                    <li>Recipes</li>
                    <li>Hotline</li>
                    <li>Icon</li>
                </ul>
            </div>
            <div className="side">
                <button className="btn login">Login</button>
                <button className="btn btn-dark sign">Sign Up</button>
            </div>
        </header>
        
    )
}

export default NavBar;