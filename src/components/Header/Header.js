import React from 'react';
import '../../App.css';
import showcaseImage from './img.png'

const Header = () => {
    return(
        <div className="showcase">
            <div className="showcase-title">
                <h1 className="title">Recipe <br /> Contest</h1>
                <p>Recipe contest are like our big, <br />
                constantly-in-progress dinner, <br />
                party- and you're invited.
                </p>
                <p><b>How they Work</b></p>
            </div>
            <div className="showcase-image">
                <img src={showcaseImage} alt="showcase" />
            </div>

        </div>
        
    )
}

export default Header;