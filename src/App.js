import React, { Component } from 'react';
import NavBar from './components/NavBar/NavBar'
import Header from './components/Header/Header'
import SideBar from './components/container/sidebar'
import MainShowcase from './components/container/mainShowcase'
import './App.css';

class App extends Component {
 render(){
   return(
    <div className="App">
      <NavBar />
      <Header />
      <SideBar />
      <MainShowcase />
  </div>
   )
 }
}

export default App;
